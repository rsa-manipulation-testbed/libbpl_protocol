//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "./cobs.h"
#include "./old_cobs.h"
#include "gtest/gtest.h"

using libbpl_protocol::ByteVector;

std::vector<ByteVector> cobs_test_data = {
    {0x1, 0x2, 0x3, 0x4},
    {0x0, 0x1, 0x2, 0x3},
    {0x0, 0x1, 0x0, 0x2, 0x3, 0x0, 0x0, 0x5}};

TEST(CobsTest, Test) {
  for (const auto td : cobs_test_data) {
    ByteVector new_cobs = libbpl_protocol::cobsEncode(td);

    uint8_t b[255];
    auto blen = old_cobsEncode(td.data(), td.size(), b);

    ByteVector old_cobs(reinterpret_cast<uint8_t *>(std::begin(b)),
                        reinterpret_cast<uint8_t *>(std::begin(b)) + blen);

    ASSERT_EQ(new_cobs, old_cobs);

    // And test decoding
    ByteVector new_decode = libbpl_protocol::cobsDecode(old_cobs);

    uint8_t c[255];
    auto clen = old_cobsDecode(old_cobs.data(), old_cobs.size(), c);

    ByteVector old_decode(reinterpret_cast<uint8_t *>(std::begin(c)),
                          reinterpret_cast<uint8_t *>(std::begin(c)) + clen);

    ASSERT_EQ(new_decode, old_decode);
  }
}
