//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <gtest/gtest.h>

#include "./autogen_test_data.h"
#include "libbpl_protocol/packet.h"

using libbpl_protocol::ByteVector;
using libbpl_protocol::Packet;
using libbpl_protocol::PacketTypes;

TEST(PacketTest, TestConstructor) {
  const uint8_t dev_id = 0x01;
  Packet packet(PacketTypes::MODE, dev_id);

  ASSERT_EQ(packet.type(), PacketTypes::MODE);
  ASSERT_EQ(packet.deviceId(), dev_id);
}

TEST(PacketTest, TestTestData) {
  for (auto const &test : testData) {
    Packet packet(static_cast<PacketTypes>(test.packet_type), test.device_id,
                  test.data);

    ByteVector buffer = packet.encode();

    ASSERT_EQ(buffer, test.buffer)
        << "Packet output does not match test data for \"" << test.description
        << "\"";

    // And decode buffer
    {
      auto d = Packet::Decode(test.buffer);

      // Assert that it's not an error
      ASSERT_TRUE(d) << "Decode error: " << d.error().msg();

      if (d) {
        auto decoded = d.value();
        ASSERT_EQ(packet.type(), decoded.type());
        ASSERT_EQ(packet.deviceId(), decoded.deviceId());
        ASSERT_EQ(packet.data(), decoded.data());
      }
    }
  }
}

// Hand crafted tests
TEST(PacketTest, CheckEncodeHandcrafted) {
  {
    BPLTestData TestPacket(
        0x03, 0x0a, {0x00, 0x00, 0x00, 0x3f},
        {0x01, 0x01, 0x01, 0x06, 0x3f, 0x03, 0x0a, 0x08, 0x1e, 0x00},
        "POSITION 0.5 to device 0x0A");

    const auto vel = 0.5;
    Packet packet(PacketTypes::POSITION, 0x0A);
    packet.push_back<float>(vel);

    auto buffer = packet.encode();

    ASSERT_EQ(buffer, TestPacket.buffer);

    ASSERT_EQ(packet.pop_front<float>(), vel);
  }
}

TEST(PacketTest, DecodeMultiplePackets) {
  {
    // This hand-constructed packet contains three BPL packets:
    //   - POSITION 0.5 to device 0x0A
    //   - REQUEST for POSITION to device 0x01
    //   - POSITION 0.5 to device 0x0A
    //
    // (these are taken from the auto-generated test data)

    // clang-format off
    ByteVector data = {0x01, 0x01, 0x01, 0x06, 0x3f, 0x03, 0x0a,
                             0x08, 0x1e, 0x00,
                       0x06, 0x03, 0x60, 0x01, 0x05, 0x52, 0x00,
                       0x01, 0x01, 0x01, 0x06, 0x3f, 0x03, 0x0a,
                             0x08, 0x1e, 0x00};
    // clang-format on

    auto result = Packet::DecodePackets(data);

    ASSERT_TRUE(result) << "Decode error: " << result.error().msg();

    if (result) {
      auto packets = result.value();
      ASSERT_EQ(packets.size(), 3)
          << "Did not decode correct number of packets";

      // Check the first and third packets
      for (auto idx : {0, 2}) {
        auto packet = packets[idx];

        ASSERT_EQ(packet.deviceId(), 0x0A);
        ASSERT_EQ(packet.type(), PacketTypes::POSITION);
        ASSERT_FLOAT_EQ(packet.pop_front<float>(), 0.5);
      }

      // Check the second packet
      {
        auto packet = packets[1];

        ASSERT_EQ(packet.deviceId(), 0x01);
        ASSERT_EQ(packet.type(), PacketTypes::REQUEST);

        // todo:  check validity of contents
      }
    }
  }
}
