//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// For testing purposes we retained this C-style COBS encode/decode functions
// pulled from Wikipedia:
// https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing#Implementation
//
// It's used to cross-check the "real" C++ implementation used in the library.
//

#include "old_cobs.h"  // NOLINT(build/include_subdir)

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

/** COBS encode data to buffer
        @param data Pointer to input data to encode
        @param length Number of bytes to encode
        @param buffer Pointer to encoded output buffer
        @return Encoded buffer length in bytes
        @note Does not output delimiter byte
*/
size_t old_cobsEncode(const void *data, size_t length, uint8_t *buffer) {
  assert(data && buffer);

  uint8_t *encode = buffer;   // Encoded byte pointer
  uint8_t *codep = encode++;  // Output code pointer
  uint8_t code = 1;           // Code value

  for (const uint8_t *byte = (const uint8_t *)data; length--; ++byte) {
    if (*byte)  // Byte not zero, write it
      *encode++ = *byte, ++code;

    if (!*byte || code == 0xff) {
      // Input is zero or block completed, restart
      *codep = code, code = 1, codep = encode;
      if (!*byte || length) ++encode;
    }
  }
  *codep = code;  // Write final code value

  return (size_t)(encode - buffer);  // NOLINT(readability/casting)
}

/** COBS decode data from buffer
        @param buffer Pointer to encoded input bytes
        @param length Number of bytes to decode
        @param data Pointer to decoded output data
        @return Number of bytes successfully decoded
        @note Stops decoding if delimiter byte is found
*/
size_t old_cobsDecode(const uint8_t *buffer, size_t length, void *data) {
  assert(buffer && data);

  const uint8_t *byte = buffer;       // Encoded input byte pointer
  uint8_t *decode = (uint8_t *)data;  // NOLINT(readability/casting)

  for (uint8_t code = 0xff, block = 0; byte < buffer + length; --block) {
    if (block) {
      // Decode block byte
      *decode++ = *byte++;
    } else {
      if (code != 0xff)  // Encoded zero, write it
        *decode++ = 0;
      block = code = *byte++;  // Next block length
      if (!code)               // Delimiter code found
        break;
    }
  }

  return (size_t)(decode - (uint8_t *)data);  // NOLINT(readability/casting)
}
