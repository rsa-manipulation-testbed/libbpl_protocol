//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <boost/asio.hpp>
#include <string>

#include "libbpl_protocol/bpl_client_common.h"
#include "libbpl_protocol/constants.h"
#include "libbpl_protocol/expected.hpp"
#include "libbpl_protocol/packet.h"
#include "libbpl_protocol/packet_types.h"

namespace libbpl_protocol {

using boost::asio::ip::udp;

using libbpl_protocol::Packet;

class SyncClient : public BplClientCommon {
 public:
  explicit SyncClient(
      const std::string &ipAddress = libbpl_protocol::DefaultBravoComputeIP,
      int port = libbpl_protocol::DefaultBravoPort);

  // Note this function waits for **ONE** packet in return
  // it will not check for additional packets.
  //
  // \todo No clean way to report failure
  // \todo No timeout
  PacketOrError send_receive(const Packet &packet);

  void send(const Packet &packet) override;

  // Simple accessors
  FloatOrError queryFloat(PacketTypes type, const uint8_t deviceId);

  FloatOrError velocity(const uint8_t deviceId) {
    return queryFloat(PacketTypes::VELOCITY, deviceId);
  }

  FloatOrError position(const uint8_t deviceId) {
    return queryFloat(PacketTypes::POSITION, deviceId);
  }

  FloatOrError current(const uint8_t deviceId) {
    return queryFloat(PacketTypes::CURRENT, deviceId);
  }

 private:
  boost::asio::io_context context_;
  udp::socket socket_;
};

}  // namespace libbpl_protocol
