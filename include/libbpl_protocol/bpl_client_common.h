//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <vector>

#include "libbpl_protocol/packet.h"

namespace libbpl_protocol {

using libbpl_protocol::Packet;

/// \brief Functions common to to synchronous and asynchronous clients
///
/// Contains helpers to generate and send packets, these call
/// virtual `send()` function provided inherited classes
/// Since the inherited class could support either synchronous or
/// asynchronous communications with the arm, these helper functions
/// **do not** expect a response from the actuator.
///
class BplClientCommon {
 public:
  BplClientCommon();

  virtual void send(const Packet &packet) = 0;

  // == Convenient / helper functions

  /// Send a position command to an actuator.
  /// Position is in rad for angular joints and mm for linear joints.
  void sendPosition(Packet::DeviceId_t device_id, float position);

  /// Send a velocity command to an actuator.
  /// Velocity is in rad/sec for angular joints and mm/sec for
  /// linear joints.
  void sendVelocity(Packet::DeviceId_t device_id, float velocity);

  /// Set the heartbeat frequency
  void setHeartbeatFrequency(Packet::DeviceId_t device_id, uint8_t rate);

  /// Set the heartbeat frequency
  void setHeartbeatSet(Packet::DeviceId_t device_id,
                       std::vector<PacketTypes> ids);

  /// Set the heartbeat frequency
  void request(Packet::DeviceId_t device_id, std::vector<PacketTypes> ids);

  /// Broadcasts "set to mode MODE_STANDBY" to all actuators
  void enable();

  /// Broadcasts "set to mode MODE_DISABLE" to all actuators
  void disable();

 private:
  bool have_warned_heartbeat_;
};

}  // namespace libbpl_protocol
