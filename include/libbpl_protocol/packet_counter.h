//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <stdint.h>

#include <algorithm>
#include <map>
#include <mutex>  // NOLINT(build/c++11)
#include <vector>

#include "libbpl_protocol/bpl_error.h"
#include "libbpl_protocol/packet_types.h"

namespace libbpl_protocol {

template <unsigned int N_JOINTS>
class PacketCounter {
 public:
  PacketCounter() : packet_count_(), total_packets_(0), global_lock_() { ; }

  ~PacketCounter() {}

  // Primary callback
  void packetRx(Packet packet) {
    std::lock_guard<std::mutex> lock(global_lock_);

    total_packets_++;

    if ((packet.deviceId() < 1) || (packet.deviceId() > N_JOINTS)) {
      invalid_joint_packet_count_[packet.type()]++;
    } else {
      packet_count_[packet.deviceId() - 1][packet.type()]++;
    }
  }

  unsigned int numPackets() const { return total_packets_; }

  void reset() {
    std::lock_guard<std::mutex> lock(global_lock_);

    total_packets_ = 0;
    invalid_joint_packet_count_.clear();

    for (auto i = 0; i < N_JOINTS; i++) {
      packet_count_[i].clear();
    }
  }

  void dump() {
    // Accumulate all keys seen thusfar
    std::vector<PacketTypes> all_packet_types;

    for (const auto pkt : invalid_joint_packet_count_) {
      all_packet_types.push_back(pkt.first);
    }

    for (auto i = 0; i < N_JOINTS; i++) {
      for (const auto pkt : packet_count_[i]) {
        all_packet_types.push_back(pkt.first);
      }
    }

    std::sort(all_packet_types.begin(), all_packet_types.end());
    auto last = std::unique(all_packet_types.begin(), all_packet_types.end());
    all_packet_types.erase(last, all_packet_types.end());

    std::cerr << "Received " << total_packets_ << " packets and "
              << all_packet_types.size() << " unique packet types" << std::endl;

    // \todo{amarburg} Clean up formatting

    // Header
    std::cerr << "          ";
    for (auto i = 0; i < N_JOINTS; i++) {
      std::cerr << "  " << static_cast<int>(i + 1) << "  ";
    }
    std::cerr << "   (unknown)" << std::endl;

    for (auto const type : all_packet_types) {
      std::cerr << to_string(type) << "  ";
      for (auto i = 0; i < N_JOINTS; i++) {
        std::cerr << static_cast<int>(packet_count_[i][type]) << "  ";
      }
      std::cerr << static_cast<int>(invalid_joint_packet_count_[type])
                << std::endl;
    }
  }

 protected:
  typedef std::map<PacketTypes, unsigned int> PacketCount;

  std::array<PacketCount, N_JOINTS> packet_count_;
  PacketCount invalid_joint_packet_count_;

  unsigned int total_packets_;

  std::mutex global_lock_;

  PacketCounter(const PacketCounter &) = delete;
  PacketCounter &operator=(const PacketCounter &) = delete;
};

}  // namespace libbpl_protocol
