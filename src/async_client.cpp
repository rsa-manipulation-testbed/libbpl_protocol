//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "libbpl_protocol/async_client.h"

#include <iostream>

namespace libbpl_protocol {

AsynchronousClient::AsynchronousClient(boost::asio::io_service &service,
                                       const std::string ipAddress,
                                       const int port)
    : socket_(service) {
  std::cerr << "Connecting to " << ipAddress << ":" << port << std::endl;
  socket_.connect(
      udp::endpoint(boost::asio::ip::address::from_string(ipAddress), port));
  std::cerr << "    ...connected" << std::endl;

  readNext();
}

void AsynchronousClient::readNext() {
  buffer_.resize(256);
  socket_.async_receive(
      boost::asio::buffer(buffer_),
      std::bind(&AsynchronousClient::onReceive, this, std::placeholders::_1,
                std::placeholders::_2));
}

void AsynchronousClient::send(const libbpl_protocol::Packet &v) {
  boost::system::error_code error;
  auto len = socket_.send(boost::asio::buffer(v.encode()));
  // \todo Ignoring any errors for now
  // std::cerr << "[ t = " << 0.0 << "] Sent " << len << " bytes" << std::endl;
}

void AsynchronousClient::onReceive(const boost::system::error_code &ec,
                                   std::size_t bytes_transferred) {
  // std::cout << "[ t = " << 0 << "] Received " << bytes_transferred
  // << " bytes" << std::endl;

  if (ec) {
    // On error, just abandon and wait for the next packet
    readNext();
    return;
  }

  buffer_.resize(bytes_transferred);

  auto p = Packet::DecodePackets(buffer_);
  if (p) {
    for (auto packet : p.value()) {
      for (auto c : packetCallbacks_) c(packet);
    }
  } else {
    std::cout << "Error: " << p.error().msg() << std::endl;
  }

  readNext();
}

}  // namespace libbpl_protocol
