//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "libbpl_protocol/sync_client.h"

#include <iostream>

namespace libbpl_protocol {

SyncClient::SyncClient(const std::string &ipAddress, int port)
    : context_(), socket_(context_) {
  socket_.connect(
      udp::endpoint(boost::asio::ip::address::from_string(ipAddress), port));
}

PacketOrError SyncClient::send_receive(const libbpl_protocol::Packet &packet) {
  libbpl_protocol::ByteVector out(packet.encode());

  auto len = socket_.send(boost::asio::buffer(out));

  libbpl_protocol::ByteVector v(256);
  len = socket_.receive(boost::asio::buffer(v));
  v.resize(len);

  return Packet::Decode(v);
}

void SyncClient::send(const libbpl_protocol::Packet &packet) {
  // Only works with messages that don't return a response
  libbpl_protocol::ByteVector out(packet.encode());
  socket_.send(boost::asio::buffer(out));
}

FloatOrError SyncClient::queryFloat(PacketTypes type, const uint8_t deviceId) {
  Packet sent_packet(PacketTypes::REQUEST, deviceId);
  sent_packet.push_back(type);

  auto received = send_receive(sent_packet);

  if (!received) return tl::make_unexpected(received.error());

  // If multiple BPL packets were received in one UDP packet,
  // only take the first
  auto rx_packet = received.value();

  if (rx_packet.type() != type)
    return tl::make_unexpected(
        BplError(BplErrorTypes::BPL_UNEXPECTED_TYPE, "Unexpected type"));

  return rx_packet.pop_front<float>();
}

}  // namespace libbpl_protocol
