//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "libbpl_protocol/packet.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "./cobs.h"
#include "./crc.h"

namespace libbpl_protocol {

Packet::Packet(PacketTypes tp, DeviceId_t device, ByteVector data)
    : _type(tp), _device(device), _data(data) {}

ByteVector Packet::encode() const {
  // std::cout << "Encoding packet with " << _data.size() << " bytes of data" <<
  // std::endl;
  ByteVector out(_data);

  out.push_back(static_cast<uint8_t>(_type));
  out.push_back(_device);

  // Length is current buffer plus two (length and CRC)
  out.push_back(out.size() + 2);

  // Calculate CRC
  auto crc = crcSlow(out.data(), out.size());
  out.push_back(crc);

  // COBS encode
  auto cobs_out = cobsEncode(out);

  // Add terminator
  cobs_out.push_back(0x00);

  // std::cout << "Total packet " << cobs_out.size() << " bytes" << std::endl;

  return cobs_out;
}

//==========

PacketOrError Packet::Decode(const ByteVector &buffer) {
  PacketsOrError result = DecodePackets(buffer);

  if (!result) {
    return tl::make_unexpected(result.error());
  }

  if (result.value().size() > 0) {
    return result.value().front();
  }

  return tl::make_unexpected(BplError(BplErrorTypes::BPL_BAD_PACKET));
}

PacketsOrError Packet::DecodePackets(const ByteVector &buffer) {
  if (buffer.size() == 0)
    return tl::make_unexpected(BplError(BplErrorTypes::BPL_BAD_PACKET));

  std::vector<Packet> packets_out;

  // Note a buffer could contain multiple packets
  ByteVector::const_iterator start = buffer.begin();
  ByteVector::const_iterator delim =
      std::find(start, buffer.end(), libbpl_protocol::Delimiter);

  while (delim != buffer.end()) {
    if (*delim != libbpl_protocol::Delimiter)
      return tl::make_unexpected(
          BplError(BplErrorTypes::BPL_BAD_PACKET, "Bad delimiter"));

    ByteVector work = cobsDecode(start, delim);

    // Check CRC
    auto buffer_crc = work.back();
    work.pop_back();

    auto crc = crcSlow(work.data(), work.size());

    if (crc != buffer_crc) {
      return tl::make_unexpected(
          BplError(BplErrorTypes::BPL_BAD_CRC, "Bad CRC"));
    }

    const auto len = work.back();
    // The length byte actually includes the CRC byte which we've already
    // dropped
    if (len != (work.size() + 1)) {
      return tl::make_unexpected(
          BplError(BplErrorTypes::BPL_UNEXPECTED_LENGTH, "Bad length"));
    }
    work.pop_back();

    uint8_t device_id = work.back();
    work.pop_back();
    uint8_t packet_type = work.back();
    work.pop_back();

    packets_out.emplace_back(
        Packet(static_cast<PacketTypes>(packet_type), device_id, work));

    // Look for the next delimiter
    start = delim + 1;
    delim = std::find(start, buffer.end(), libbpl_protocol::Delimiter);
  }

  return packets_out;
}

//===================================================================

void Packet::VerbosePacketRx(Packet packet) {
  std::cout << "Device " << static_cast<int>(packet.deviceId())
            << " packet type " << to_string(packet.type()) << " ("
            << static_cast<int>(packet.type()) << ")" << std::endl;

  if (packet.type() == PacketTypes::MODE) {
    std::cout << "    Mode " << static_cast<int>(packet.pop_front<uint8_t>())
              << std::endl;
  } else if (packet.type() == PacketTypes::HEARTBEAT_FREQUENCY) {
    std::cout << "    Heartbeat freq "
              << static_cast<int>(packet.pop_front<uint8_t>()) << " Hz"
              << std::endl;
  } else if (packet.type() == PacketTypes::HEARTBEAT_SET) {
    const auto sz = packet.dataSize();
    std::cout << "    Set contains " << static_cast<int>(sz) << " entries: ";
    for (size_t i = 0; i < sz; ++i) {
      std::cout << std::hex << static_cast<int>(packet.pop_front<uint8_t>())
                << " ";
    }
    std::cout << std::dec << std::endl;
  } else if (packet.type() == PacketTypes::SOFTWARE_VERSION) {
    std::cout << "    Software version "
              << static_cast<int>(packet.pop_front<uint8_t>()) << "."
              << static_cast<int>(packet.pop_front<uint8_t>()) << "."
              << static_cast<int>(packet.pop_front<uint8_t>()) << std::endl;
  } else if (packet.type() == PacketTypes::SERIAL_NUMBER) {
    std::cout << "    Serial Number " << packet.pop_front<float>() << std::endl;
  } else if (packet.type() == PacketTypes::MODEL_NUMBER) {
    std::cout << "    Model Number " << packet.pop_front<float>() << std::endl;
  } else if (packet.type() == PacketTypes::POSITION_LIMITS) {
    std::cout << "   Position limits: " << packet.pop_front<float>() << "  "
              << packet.pop_front<float>() << std::endl;
  } else if (packet.type() == PacketTypes::CURRENT_LIMITS) {
    std::cout << "   Current limits: " << packet.pop_front<float>() << "  "
              << packet.pop_front<float>() << std::endl;
  }
}

}  // namespace libbpl_protocol
