//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "cobs.h"  // NOLINT(build/include_subdir)

#include <iostream>

namespace libbpl_protocol {

using libbpl_protocol::ByteVector;

ByteVector cobsEncode(const ByteVector &const_in) {
  // Create working copy with 0x00 prepended to front of buffer
  ByteVector in = {0x00};
  in.insert(in.end(), const_in.begin(), const_in.end());

  ByteVector out;
  out.reserve(in.size());

  // We'll be bad and use indices not iterators
  for (size_t i = 0; i < in.size(); i++) {
    if (in[i] != 0x00) {
      // If non-zero just push to the output
      out.push_back(in[i]);
    } else {
      // Otherwise lookahead
      size_t j = i + 1;
      for (; j < in.size(); j++) {
        if (in[j] == 0x00) {
          out.push_back(j - i);
          break;
        }
      }

      // If you exited the loop without finding 0x00
      // push bytes to the end of the packet
      if (j >= in.size()) {
        out.push_back(in.size() - i);
      }
    }
  }

  return out;
}

libbpl_protocol::ByteVector cobsDecode(
    const libbpl_protocol::ByteVector &const_in) {
  return cobsDecode(const_in.begin(), const_in.end());
}

libbpl_protocol::ByteVector cobsDecode(
    libbpl_protocol::ByteVector::const_iterator begin,
    libbpl_protocol::ByteVector::const_iterator end) {
  ByteVector out;

  auto count = *begin;
  begin++;

  while (begin != end) {
    if (--count == 0) {
      out.push_back(0x00);
      count = *begin;
    } else {
      out.push_back(*begin);
    }

    begin++;
  }

  return out;
}

}  // namespace libbpl_protocol
