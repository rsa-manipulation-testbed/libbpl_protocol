//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "libbpl_protocol/bpl_client_common.h"

#include <iostream>

namespace libbpl_protocol {

BplClientCommon::BplClientCommon() : have_warned_heartbeat_(false) { ; }

void BplClientCommon::sendPosition(uint8_t device_id, float position) {
  Packet sent(PacketTypes::POSITION, device_id);
  sent.push_back<float>(position);
  send(sent);
}

void BplClientCommon::sendVelocity(uint8_t device_id, float velocity) {
  Packet sent(PacketTypes::VELOCITY, device_id);
  sent.push_back<float>(velocity);
  send(sent);
}

/// Set the heartbeat frequency
void BplClientCommon::setHeartbeatFrequency(Packet::DeviceId_t device_id,
                                            uint8_t rate) {
  if (rate == 0) {
    if (!have_warned_heartbeat_) {
      std::cerr
          << "WARN: Setting heartbeat frequency to 0 will break heatbeat "
             "functionality.   Calling setHeartbeatSet with empty set instead";
      have_warned_heartbeat_ = true;
    }
    setHeartbeatSet(device_id, {});
  } else {
    // Some documentation claims only a set number of rates work (e.g., 1, 5,
    // 50Hz) If true, validate that here

    Packet pkt(PacketTypes::HEARTBEAT_FREQUENCY, device_id);
    pkt.push_back<uint8_t>(rate);
    send(pkt);
  }
}

/// Set the heartbeat frequency
void BplClientCommon::setHeartbeatSet(Packet::DeviceId_t device_id,
                                      std::vector<PacketTypes> ids) {
  // How to pass error?
  if (ids.size() > 10) {
    return;
  }

  Packet pkt(PacketTypes::HEARTBEAT_SET, device_id);
  for (const auto id : ids) {
    pkt.push_back(id);
  }

  // I think the HEARTBEAT_SET is ignored if the data is length 0.
  //
  // This call ensures that if ids.size() == 0, one 0x00 is pushed into data.
  // (this removes all entries from the heartbeat set and stops the heartbeat)
  //
  // If ids.size() > 0 this call has no effect
  pkt.pad_data(1);

  send(pkt);
}

/// Set the heartbeat frequency
void BplClientCommon::request(Packet::DeviceId_t device_id,
                              std::vector<PacketTypes> ids) {
  // How to pass error?
  if (ids.size() > 10) {
    return;
  }

  Packet pkt(PacketTypes::REQUEST, device_id);
  for (const auto id : ids) {
    pkt.push_back(id);
  }
  send(pkt);
}

// Broadcasts "set to mode MODE_STANDBY" to all actuators
void BplClientCommon::enable() {
  Packet sent(PacketTypes::MODE, libbpl_protocol::BroadcastDeviceId);
  sent.push_back(Mode::MODE_STANDBY);
  send(sent);
}

// Broadcasts "set to mode MODE_DISABLE" to all actuators
void BplClientCommon::disable() {
  Packet sent(PacketTypes::MODE, libbpl_protocol::BroadcastDeviceId);
  sent.push_back(Mode::MODE_DISABLE);
  send(sent);
}

}  // namespace libbpl_protocol
