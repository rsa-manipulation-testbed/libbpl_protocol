# bpl_watchdogd

`bpl_watchdogd` is a safety watchdog service that intervenes between a Reach robotics actuator/arm and one or more clients which want to control the arm or observe communication with the arm.

It must be run on a computer that has access to both the client network and the arm network (e.g. a computer with multiple ethernet interfaces or multiple virtual IP addresses).  A secondary benefit of this is that the daemon can route data between the client and arm networks, removing the need for clients to be configured to be on the default arm 192.168.2.x network.


The daemon runs two primary state machines:

![](bpl_watchdogd_state_diagram.jpg)

The **client state machine**

* Listens on UDP port 6789 (same as the arm itself; existing clients will work without modification **except** their IP address must be pointed to the `bpl_watchdogd` host)
* Any client which sends data to the service will be added to a subscriber list and receive a copy of all subsequent packets received from the arm.  Note clients **must** send *something* (even a single byte, it does not need to be a meaningful BPL packet)  for the daemon to recognize their presence.
  * The daemon will track up to 32 clients, sorted by the most-recently-heard-from.
* Any non-empty packets sent to the service are forwarded to the arm -- the service **does not** perform access control for the arm,  **but it will print a warning if it gets consecutive packets to the arm sent from different clients.**
* If no data is received from any clients for 1 second, it will halt the arm by sending `Set MODE to STANDBY` to all joints.

* The watchdog also provides two "monitoring" port.  Both ports function similar to 6789, in that clients must send *something* to register with the daemon.  Once registered:

  * Port 6788 echos packets from the arm.   This stream is identical to port 6789 but is *read only*.
  * Port 6787 echos packets received from *any* client on 6789.   This port echos client data *even if the e-stop function is active.*


The **arm state machine**

* `bpl_watchdogd` expects to be the only process communicating with the arm.
* Any data received from the arm is forwarded to all registered clients.
* If no data is received from the arm for 0.25 seconds, it will send a `MODE` query packet to the arm in an attempt to provoke a response from the arm.
* If no data is received from the arm for 1.0 second, it will send halt the arm then attempt to re-connect to the arm.  This should handle the case where the arm has been rebooted and the UDP connection reset.

In addition:

* If a non-empty packet is sent to the "e-stop" UDP port (defaults to 6790), it will halt the arm as above **and** stop forwarding any client traffic to the arm.   This e-stop is removed by sending a packet containing "RUN" to the same port.  An example command line for halting the arm:

```
echo "x" | nc -u localhost 6790
```

and for re-starting the arm:

```
echo "RUN" | nc -u localhost 6790
```

* If a non-empty packet is sent to the "status" UDP port (defaults to 6791), the service will subsequently send a set of NMEA-style messages which describe the service state and statistics.


# Building

To build:

```
mkdir build && cd build
cmake ..
make
```

# Debian package

> **NOTE:** There are bugs with running the daemon as a system service.   Generally, __do not__ do this.  Run the watchdog on the command line.

The [CMakeLists.txt](CMakeLists.txt) can generate a `.deb` package.  To generate, use `cpack`:

```
cpack -G DEB
```

The resulting `.deb` file will be in the `_packages/` directory.

The `deb` installs the program as a systemctl service which can be controller with `systemctl` e.g.:

```
sudo systemctl status bpl_watchdogd.service
```

# TODOs

* Add command-line configuration to the daemon.

# License

As part of `libbpl_protocol`, `bpl_watchdogd` is released under the [BSD 3-Clause license](../LICENSE)
