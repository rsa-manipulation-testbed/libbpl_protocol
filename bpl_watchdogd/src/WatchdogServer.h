/******************************************************************************
 * Software License Agreement (BSD License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of the Copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#pragma once

#include <array>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <string>

#include "./EndpointList.h"

using boost::asio::ip::udp;

namespace bpl_watchdogd {

class WatchdogServer {
 public:
  static const int MaxClients = 4;

  explicit WatchdogServer(
      boost::asio::io_context &io_context,  // NOLINT(runtime/references)
      const std::string &arm_addr, int port, int client_port = 6789,
      int listen_only_port = 6788, int client_echo_port = 6787,
      int estop_port = 6790, int status_port = 6791);
  ~WatchdogServer();

  bool connectArm();

 protected:
  void haltArm();

  std::string arm_addr_;
  int arm_port_;

  bool block_arm_cmds_;

  // This state variable is used to track the current state of communications
  // with the arm. Primarily used to show appropriate console output on state
  // transitions (e.g., print a message _once_ when good communications is
  // (re)established)
  enum class ArmCommsState {
    WaitingForResponse,
    GoodCommunications,
    SelfQuerying,
    NoResponse
  } arm_comms_state_;

  udp::socket client_socket_;
  udp::endpoint client_endpoint_, last_sender_;
  std::array<char, 128> client_recv_buffer_;
  EndpointList<MaxClients> client_endpoints_, listen_only_endpoints_,
      client_echo_endpoints_;

  udp::socket listen_only_socket_;
  udp::endpoint listen_only_endpoint_;
  std::array<char, 128> listen_only_recv_buffer_;

  udp::socket client_echo_socket_;
  udp::endpoint client_echo_endpoint_;
  std::array<char, 128> client_echo_recv_buffer_;

  udp::socket arm_socket_;
  std::array<char, 128> arm_recv_buffer_;

  udp::socket estop_socket_;
  udp::endpoint estop_endpoint_;
  std::array<char, 128> estop_recv_buffer_;

  udp::socket status_socket_;
  udp::endpoint status_endpoint_;
  std::array<char, 128> status_recv_buffer_;
  EndpointList<MaxClients> status_endpoints_;

  int packets_to_arm_, packets_from_arm_;
  int bytes_to_arm_, bytes_from_arm_;

  // Timers
  void refreshClientTimeout();
  void refreshArmTimeout();

  boost::asio::deadline_timer client_timer_, arm_timer_, arm_ping_timer_,
      status_timer_;
  boost::posix_time::time_duration client_timeout_, arm_timeout_,
      arm_ping_timeout_, status_timeout_;

  // ASIO callbacks
  void startClientReceive();
  void onClientReceive(const boost::system::error_code &error, std::size_t);
  void onClientSent(const udp::endpoint &client,
                    const boost::system::error_code &error, std::size_t);

  void startListenOnlyReceive();
  void onListenOnlyReceive(const boost::system::error_code &error, std::size_t);
  void onListenOnlySent(const udp::endpoint &client,
                        const boost::system::error_code &error, std::size_t);

  void startClientEchoReceive();
  void onClientEchoReceive(const boost::system::error_code &error, std::size_t);
  void onClientEchoSent(const udp::endpoint &client,
                        const boost::system::error_code &error, std::size_t);

  void startArmReceive();
  void onArmReceive(const boost::system::error_code &error, std::size_t);
  void onArmSent(const boost::system::error_code &error, std::size_t bytes_out);

  void startEstopReceive();
  void onEstopReceive(const boost::system::error_code &error, std::size_t);

  void startStatusReceive();
  void onStatusReceive(const boost::system::error_code &error, std::size_t);
  void onStatusSent(const udp::endpoint &client,
                    const boost::system::error_code &error, std::size_t);
  void sendStatus(const std::string &msg);
  void setStatusTimer(void);

  void onArmPingTimeout(const boost::system::error_code &e);
  void onArmTimeout(const boost::system::error_code &e);
  void onClientTimeout(const boost::system::error_code &e);
  void onStatusTimeout(const boost::system::error_code &e);

  WatchdogServer(const WatchdogServer &) = delete;
  WatchdogServer &operator=(const WatchdogServer &) = delete;
};

}  // namespace bpl_watchdogd
