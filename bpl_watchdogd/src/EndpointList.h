/******************************************************************************
 * Software License Agreement (BSD License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of the Copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#pragma once

#include <boost/asio.hpp>
#include <list>

using boost::asio::ip::udp;

namespace bpl_watchdogd {

/// EndpointList is a size-bounded queue of UDP endpoints;
/// When existing endpoints are re-registered, they move to the
/// front of the queue.  When necessary, entries are dropped from
/// the back of the queue (the least-recently seen/registered).
///
template <int MaxClients>
class EndpointList {
 public:
  typedef std::list<udp::endpoint> EndpointList_t;

  EndpointList() {}
  ~EndpointList() {}

  /// Add the given endpoint to the list
  /// If it already exists, move it to the front of the list.
  ///
  void registerEndpoint(const udp::endpoint &endpoint) {
    auto elem = std::find(endpoints_.begin(), endpoints_.end(), endpoint);

    if (elem == endpoints_.end()) {
      endpoints_.push_front(endpoint);
    } else {
      // Move client to the front of the list so the list is ordered
      // by time since last contact from client
      endpoints_.erase(elem);
      endpoints_.push_front(endpoint);
    }

    // Drop entries to stay under the max size
    while (endpoints_.size() > MaxClients) endpoints_.pop_back();
  }

  void unregisterEndpoint(const udp::endpoint &endpoint) {
    auto elem = std::find(endpoints_.begin(), endpoints_.end(), endpoint);

    if (elem != endpoints_.end()) {
      endpoints_.erase(elem);
    }
  }

  EndpointList_t::iterator begin() { return endpoints_.begin(); }
  EndpointList_t::const_iterator begin() const { return endpoints_.begin(); }

  EndpointList_t::iterator end() { return endpoints_.end(); }
  EndpointList_t::const_iterator end() const { return endpoints_.end(); }

 private:
  EndpointList_t endpoints_;
};

}  // namespace bpl_watchdogd
