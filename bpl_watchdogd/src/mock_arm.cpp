/******************************************************************************
 * Software License Agreement (BSD License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of the Copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <iostream>

#include "WatchdogServer.h"

namespace bpl_mock {

class MockArm {
 public:
  explicit MockArm(
      boost::asio::io_context &io_context,  // NOLINT(runtime/references)
      int client_port = 6789)
      : socket_(io_context, udp::endpoint(udp::v4(), client_port)) {
    startReceive();
  }

  ~MockArm() {}

 protected:
  udp::socket socket_;
  udp::endpoint endpoint_;
  boost::array<char, 128> buffer_;

  void startReceive() {
    socket_.async_receive_from(
        boost::asio::buffer(buffer_), endpoint_,
        boost::bind(&MockArm::handleReceive, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  }

  void handleReceive(const boost::system::error_code &error,
                     std::size_t num_bytes) {
    if (!error) {
      std::cout << "Received " << num_bytes << " bytes" << std::endl;
      socket_.send_to(boost::asio::buffer(buffer_, num_bytes), endpoint_);

      startReceive();
    }
  }

  MockArm(const MockArm &) = delete;
  MockArm &operator=(const MockArm &) = delete;
};
}  // namespace bpl_mock

int main(int argc, char **argv) {
  try {
    boost::asio::io_context io_context;
    bpl_mock::MockArm server(io_context, 5789);

    io_context.run();
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
  }

  exit(0);
}
