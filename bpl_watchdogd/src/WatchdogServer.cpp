/******************************************************************************
 * Software License Agreement (BSD License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of the Copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include "WatchdogServer.h"

#include <algorithm>
#include <boost/bind/bind.hpp>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "libbpl_protocol/packet.h"

namespace bpl_watchdogd {

using libbpl_protocol::Packet;

std::string outputPreface() {
  std::ostringstream out;

  std::time_t t = std::time(nullptr);
  std::tm tm = *std::localtime(&t);

  out << "[" << std::put_time(&tm, "%H:%M:%S") << "] ";

  return out.str();
}

WatchdogServer::WatchdogServer(boost::asio::io_context &io_context,
                               const std::string &arm_addr, int port,
                               int client_port, int listen_only_port,
                               int client_echo_port, int estop_port,
                               int status_port)
    : arm_addr_(arm_addr),
      arm_port_(port),
      block_arm_cmds_(false),
      arm_comms_state_(ArmCommsState::WaitingForResponse),
      client_socket_(io_context, udp::endpoint(udp::v4(), client_port)),
      last_sender_(),
      listen_only_socket_(io_context,
                          udp::endpoint(udp::v4(), listen_only_port)),
      client_echo_socket_(io_context,
                          udp::endpoint(udp::v4(), client_echo_port)),
      arm_socket_(io_context),
      estop_socket_(io_context, udp::endpoint(udp::v4(), estop_port)),
      status_socket_(io_context, udp::endpoint(udp::v4(), status_port)),
      client_timer_(io_context),
      arm_timer_(io_context),
      arm_ping_timer_(io_context),
      status_timer_(io_context),
      // Fixed timeouts for now
      client_timeout_(boost::posix_time::milliseconds(1000)),
      arm_timeout_(boost::posix_time::milliseconds(1000)),
      arm_ping_timeout_(boost::posix_time::milliseconds(250)),
      status_timeout_(boost::posix_time::milliseconds(1000)) {
  std::cout << ">>      Clients connect to port " << client_port << std::endl;
  std::cout << ">>  Listen-only connect to port " << listen_only_port
            << std::endl;
  std::cout << ">>Client data is echoed to port " << client_echo_port
            << std::endl;
  std::cout << ">> To E-Stop, send data to port " << estop_port << std::endl;
  std::cout << ">>       Status updates on port " << status_port << std::endl;

  startEstopReceive();
  startClientReceive();
  startListenOnlyReceive();
  startClientEchoReceive();
  startStatusReceive();

  setStatusTimer();
}

WatchdogServer::~WatchdogServer() {}

void WatchdogServer::haltArm() {
  // std::cerr << "Stopping arm!" << std::endl;

  const uint8_t DeviceId = 0xFF;
  Packet packet(libbpl_protocol::PacketTypes::MODE, DeviceId);
  packet.push_back(libbpl_protocol::Mode::MODE_STANDBY);

  arm_socket_.async_send(
      boost::asio::buffer(packet.encode()),
      std::bind(&WatchdogServer::onArmSent, this, std::placeholders::_1,
                std::placeholders::_2));
}

void WatchdogServer::refreshClientTimeout() {
  if (client_timeout_.total_milliseconds() <= 0) {
    client_timer_.cancel();
    return;
  }

  client_timer_.expires_from_now(client_timeout_);
  client_timer_.async_wait(
      std::bind(&WatchdogServer::onClientTimeout, this, std::placeholders::_1));
}

//========================================================================
//
// This is the "arm state machine"

// ~~~ Arm callbacks ~~~
bool WatchdogServer::connectArm() {
  try {
    if (arm_socket_.is_open()) {
      arm_socket_.close();
    }

    std::cerr << "Connecting to arm at " << arm_addr_ << ":" << arm_port_
              << std::endl;
    arm_socket_.connect(udp::endpoint(
        boost::asio::ip::address::from_string(arm_addr_), arm_port_));

    refreshArmTimeout();

    startArmReceive();
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
    return false;
  }

  return true;  // success
}

void WatchdogServer::refreshArmTimeout() {
  if (arm_timeout_.total_milliseconds() <= 0) {
    arm_timer_.cancel();
    return;
  }

  arm_timer_.expires_from_now(arm_timeout_);
  arm_timer_.async_wait(
      std::bind(&WatchdogServer::onArmTimeout, this, std::placeholders::_1));

  arm_ping_timer_.expires_from_now(arm_ping_timeout_);
  arm_ping_timer_.async_wait(std::bind(&WatchdogServer::onArmPingTimeout, this,
                                       std::placeholders::_1));
}

void WatchdogServer::startArmReceive() {
  arm_socket_.async_receive(
      boost::asio::buffer(arm_recv_buffer_),
      std::bind(&WatchdogServer::onArmReceive, this, std::placeholders::_1,
                std::placeholders::_2));
}

void WatchdogServer::onArmReceive(const boost::system::error_code &error,
                                  std::size_t bytes_in) {
  if (!error) {
    packets_from_arm_++;
    bytes_from_arm_ += bytes_in;

    for (auto const &client : client_endpoints_) {
      client_socket_.async_send_to(
          boost::asio::buffer(arm_recv_buffer_, bytes_in), client,
          std::bind(&WatchdogServer::onClientSent, this, client,
                    std::placeholders::_1, std::placeholders::_2));
    }

    for (auto const &client : listen_only_endpoints_) {
      listen_only_socket_.async_send_to(
          boost::asio::buffer(arm_recv_buffer_, bytes_in), client,
          std::bind(&WatchdogServer::onListenOnlySent, this, client,
                    std::placeholders::_1, std::placeholders::_2));
    }
    refreshArmTimeout();

    if (arm_comms_state_ != ArmCommsState::GoodCommunications &&
        arm_comms_state_ != ArmCommsState::SelfQuerying) {
      arm_comms_state_ = ArmCommsState::GoodCommunications;
    }

    startArmReceive();
  }
}

void WatchdogServer::onArmSent(const boost::system::error_code &error,
                               std::size_t bytes_out) {
  if (!error) {
    packets_to_arm_++;
    bytes_to_arm_ += bytes_out;
  }
}

void WatchdogServer::onArmTimeout(const boost::system::error_code &e) {
  if (e != boost::asio::error::operation_aborted) {
    std::cout << outputPreface() << "No data from arm in "
              << (arm_timeout_.total_milliseconds() / 1000.0)
              << " seconds, halting arm and attempting to reconnect"
              << std::endl;
    arm_comms_state_ = ArmCommsState::NoResponse;
    haltArm();

    arm_timer_.cancel();

    // Attempt to (re)connect to the arm
    connectArm();
  }
}

void WatchdogServer::onArmPingTimeout(const boost::system::error_code &e) {
  if (e != boost::asio::error::operation_aborted) {
    // Timer was not cancelled, take necessary action.
    // Try to get response from arm

    if (arm_comms_state_ != ArmCommsState::SelfQuerying) {
      std::cout << outputPreface() << "No data from arm in "
                << (arm_ping_timeout_.total_milliseconds() / 1000.0)
                << " seconds, querying the arm" << std::endl;
    }

    arm_comms_state_ = ArmCommsState::SelfQuerying;

    const uint8_t DeviceId = 0x01;
    Packet packet(libbpl_protocol::PacketTypes::REQUEST, DeviceId);
    packet.push_back(libbpl_protocol::PacketTypes::MODE);

    arm_socket_.async_send(
        boost::asio::buffer(packet.encode()),
        std::bind(&WatchdogServer::onArmSent, this, std::placeholders::_1,
                  std::placeholders::_2));
  }
}

//========================================================================
//
// This is the "client state machine"

void WatchdogServer::startClientReceive() {
  client_socket_.async_receive_from(
      boost::asio::buffer(client_recv_buffer_), client_endpoint_,
      boost::bind(&WatchdogServer::onClientReceive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void WatchdogServer::onClientReceive(const boost::system::error_code &error,
                                     std::size_t bytes_in) {
  if (!error) {
    // \todo How to check if last_sender_ has been set?
    if ((last_sender_.port() > 0) && (client_endpoint_ != last_sender_)) {
      std::cerr << outputPreface() << "Client changed IP address.  New sender: "
                << client_endpoint_ << std::endl;
    }

    client_endpoints_.registerEndpoint(client_endpoint_);

    if (block_arm_cmds_) {
      std::cout
          << outputPreface() << "Got " << bytes_in
          << " bytes from client, but the arm has been E-stopped, not sending"
          << std::endl;

    } else {
      arm_socket_.async_send(
          boost::asio::buffer(client_recv_buffer_, bytes_in),
          std::bind(&WatchdogServer::onArmSent, this, std::placeholders::_1,
                    std::placeholders::_2));
      refreshClientTimeout();
    }

    last_sender_ = client_endpoint_;

    for (auto const &client : client_echo_endpoints_) {
      client_echo_socket_.async_send_to(
          boost::asio::buffer(client_recv_buffer_, bytes_in), client,
          std::bind(&WatchdogServer::onClientEchoSent, this, client,
                    std::placeholders::_1, std::placeholders::_2));
    }

    arm_comms_state_ = ArmCommsState::WaitingForResponse;

    startClientReceive();
  }
}

void WatchdogServer::onClientSent(const udp::endpoint &client,
                                  const boost::system::error_code &error,
                                  std::size_t bytes_out) {
  if (error) {
    std::cerr << outputPreface() << "Error talking to client "
              << client.address() << ", unregistering" << std::endl;

    // Unregister on **any** error for now
    client_endpoints_.unregisterEndpoint(client);
  }
}

void WatchdogServer::onClientTimeout(const boost::system::error_code &e) {
  if (e != boost::asio::error::operation_aborted) {
    // Timer was not cancelled, take necessary action.
    std::cout << outputPreface() << "No data from client in "
              << (client_timeout_.total_milliseconds() / 1000.0)
              << " seconds, halting arm" << std::endl;
    haltArm();

    client_timer_.cancel();
  }
}

//========================================================================
//
// Listen only clients

void WatchdogServer::startListenOnlyReceive() {
  listen_only_socket_.async_receive_from(
      boost::asio::buffer(listen_only_recv_buffer_), listen_only_endpoint_,
      boost::bind(&WatchdogServer::onListenOnlyReceive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void WatchdogServer::onListenOnlyReceive(const boost::system::error_code &error,
                                         std::size_t bytes_in) {
  if (!error) {
    listen_only_endpoints_.registerEndpoint(listen_only_endpoint_);
    startListenOnlyReceive();
  }
}

void WatchdogServer::onListenOnlySent(const udp::endpoint &client,
                                      const boost::system::error_code &error,
                                      std::size_t bytes_out) {
  if (error) {
    // Unregister on **any** error for now
    listen_only_endpoints_.unregisterEndpoint(client);
  }
}

//========================================================================
//
// Client echo clients

void WatchdogServer::startClientEchoReceive() {
  client_echo_socket_.async_receive_from(
      boost::asio::buffer(client_echo_recv_buffer_), client_echo_endpoint_,
      boost::bind(&WatchdogServer::onClientEchoReceive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void WatchdogServer::onClientEchoReceive(const boost::system::error_code &error,
                                         std::size_t bytes_in) {
  if (!error) {
    client_echo_endpoints_.registerEndpoint(client_echo_endpoint_);
    startClientEchoReceive();
  }
}

void WatchdogServer::onClientEchoSent(const udp::endpoint &client,
                                      const boost::system::error_code &error,
                                      std::size_t bytes_out) {
  if (error) {
    // Unregister on **any** error for now
    client_echo_endpoints_.unregisterEndpoint(client);
  }
}

//========================================================================
//
// E-stop functions

void WatchdogServer::startEstopReceive() {
  estop_socket_.async_receive_from(
      boost::asio::buffer(estop_recv_buffer_), estop_endpoint_,
      boost::bind(&WatchdogServer::onEstopReceive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void WatchdogServer::onEstopReceive(const boost::system::error_code &error,
                                    std::size_t bytes_in) {
  if (!error) {
    const std::array<char, 3> StartToken = {'R', 'U', 'N'};

    if (std::equal(StartToken.begin(), StartToken.end(),
                   estop_recv_buffer_.begin())) {
      // Only send debug message if cmds are currently blocked
      // (to reduce confusion)
      if (block_arm_cmds_ == true)
        std::cout << outputPreface()
                  << "E-STOP:  Re-enabling communication to arm" << std::endl;

      block_arm_cmds_ = false;
    } else {
      std::cout << outputPreface()
                << "E-STOP:  Halting arm and disabling arm communications"
                << std::endl;

      block_arm_cmds_ = true;
      haltArm();
    }

    startEstopReceive();
  }
}

//========================================================================
//
// Status functions

void WatchdogServer::startStatusReceive() {
  status_socket_.async_receive_from(
      boost::asio::buffer(status_recv_buffer_), status_endpoint_,
      boost::bind(&WatchdogServer::onStatusReceive, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
}

void WatchdogServer::onStatusReceive(const boost::system::error_code &error,
                                     std::size_t bytes_in) {
  if (!error) {
    // std::cout << "Adding status endpoint " << status_endpoint_.address() <<
    // std::endl;
    status_endpoints_.registerEndpoint(status_endpoint_);
    startStatusReceive();
  }
}

void WatchdogServer::onStatusSent(const udp::endpoint &client,
                                  const boost::system::error_code &error,
                                  std::size_t bytes_in) {
  if (error) {
    // Unregister on **any** error for now
    status_endpoints_.unregisterEndpoint(client);
  }
}

void WatchdogServer::sendStatus(const std::string &msg) {
  const auto outbuf = boost::asio::buffer(msg);

  for (auto const &client : status_endpoints_) {
    status_socket_.async_send_to(
        outbuf, client,
        std::bind(&WatchdogServer::onStatusSent, this, client,
                  std::placeholders::_1, std::placeholders::_2));
  }
}

void WatchdogServer::setStatusTimer() {
  status_timer_.expires_from_now(status_timeout_);
  status_timer_.async_wait(
      std::bind(&WatchdogServer::onStatusTimeout, this, std::placeholders::_1));
}

void WatchdogServer::onStatusTimeout(const boost::system::error_code &e) {
  if (e != boost::asio::error::operation_aborted) {
    // Construct a STATUS message
    std::stringstream output;
    output << "$STATUS,";

    if (block_arm_cmds_) {
      output << "STOP,";
    } else {
      output << "RUN,";
    }

    if (arm_comms_state_ == ArmCommsState::WaitingForResponse) {
      output << "WaitingForResponse,";
    } else if (arm_comms_state_ == ArmCommsState::GoodCommunications) {
      output << "GoodComms,";
    } else if (arm_comms_state_ == ArmCommsState::SelfQuerying) {
      output << "SelfQuerying,";
    } else if (arm_comms_state_ == ArmCommsState::NoResponse) {
      output << "NoResponse,";
    } else {
      output << "Unknown";
    }

    output << packets_to_arm_ << "," << bytes_to_arm_ << ","
           << packets_from_arm_ << "," << bytes_from_arm_;

    packets_to_arm_ = 0;
    bytes_to_arm_ = 0;
    packets_from_arm_ = 0;
    bytes_from_arm_ = 0;

    output << "\n";
    sendStatus(output.str());
  }

  setStatusTimer();
}

}  // namespace bpl_watchdogd
