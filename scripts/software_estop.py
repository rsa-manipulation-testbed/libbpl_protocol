"""
Generates a software e-stop button on screen.
Requires that bplwatchdog is running.
"""

import sys
from PyQt5.QtWidgets import QApplication, QPushButton, QVBoxLayout, QWidget
from PyQt5.QtCore import Qt
import socket
import select
import signal

signal.signal(signal.SIGINT, signal.SIG_DFL)


class BravoEStop(QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        stop_button = QPushButton("STOP!", self)
        stop_button.setStyleSheet("background-color: red; font-size: 24px;")
        stop_button.setFixedSize(300, 200)
        stop_button.clicked.connect(self.stop_bravo)

        run_button = QPushButton("RUN", self)
        run_button.setStyleSheet("background-color: green; font-size: 12px;")
        run_button.clicked.connect(self.restart_bravo)

        vbox = QVBoxLayout()
        vbox.addWidget(stop_button, alignment=Qt.AlignCenter)
        vbox.addWidget(run_button, alignment=Qt.AlignCenter)

        self.setLayout(vbox)
        self.setWindowTitle("Bravo Software E-STOP")
        self.setGeometry(100, 100, 300, 200)

    def run(self):
        self.show()

    def stop_bravo(self):
        print("Stopping Bravo...")
        self.send_and_receive_packet("x", port=6790, receive=False)

    def restart_bravo(self):
        print("Restarting Bravo Communications")
        self.send_and_receive_packet("RUN", port=6790, receive=False)

    def send_and_receive_packet(self, data, port, receive):
        host = "localhost"

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.sendto(data.encode(), (host, port))
            if receive:
                s.setblocking(0)
                ready = select.select([s], [], [], 1)
                if ready[0]:
                    received_data, _ = s.recvfrom(1024)  # Adjust buffer size as needed
                    return received_data.decode()
            else:
                return None


def main():
    app = QApplication(sys.argv)
    estop = BravoEStop()
    estop.run()

    try:
        sys.exit(app.exec_())
    except KeyboardInterrupt:
        print("Ctrl+C pressed. Exiting...")
        sys.exit(0)


if __name__ == "__main__":
    main()
