//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <iostream>

using boost::asio::ip::udp;
using std::cout;
using std::endl;
using std::string;

#include "libbpl_protocol/bpl_error.h"
#include "libbpl_protocol/constants.h"
#include "libbpl_protocol/expected.hpp"
#include "libbpl_protocol/packet.h"

using libbpl_protocol::Packet;
using libbpl_protocol::PacketOrError;
using libbpl_protocol::PacketTypes;

// Note this function waits for **ONE** packet in return
// it will not check for additional packets.
//
// \todo No clean way to report failure
// \todo No timeout
PacketOrError send_receive(udp::socket &socket,  // NOLINT(runtime/references)
                           const libbpl_protocol::Packet &packet) {
  libbpl_protocol::ByteVector out(packet.encode());

  auto len = socket.send(boost::asio::buffer(out));
  std::cerr << "Sent " << len << " bytes to arm" << std::endl;

  libbpl_protocol::ByteVector v(256);
  len = socket.receive(boost::asio::buffer(v));
  v.resize(len);

  std::cout << "Received " << v.size() << " bytes from arm" << std::endl;

  return Packet::Decode(v);
}

int main(int argc, char **argv) {
  const string ipAddress(libbpl_protocol::DefaultBravoComputeIP);
  const unsigned int port = libbpl_protocol::DefaultBravoPort;
  boost::asio::io_context context;

  udp::socket socket(context);
  socket.connect(
      udp::endpoint(boost::asio::ip::address::from_string(ipAddress), port));

  const uint8_t maxDeviceId = 0x07;
  for (uint8_t deviceId = 1; deviceId <= maxDeviceId; deviceId++) {
    Packet sent_packet(PacketTypes::REQUEST, deviceId);
    sent_packet.push_back(PacketTypes::SERIAL_NUMBER);

    auto received = send_receive(socket, sent_packet);

    if (received) {
      auto rx_packet = received.value();
      if (rx_packet.type() == PacketTypes::SERIAL_NUMBER) {
        // Serial number is encoded as a single float
        std::cout << "Serial number for id " << static_cast<int>(deviceId)
                  << " is " << rx_packet.pop_front<float>() << std::endl;
      } else {
        std::cerr << "Got unexpected packet type " << std::hex
                  << static_cast<int>(rx_packet.type()) << std::endl;
      }
    } else {
      std::cout << "Error: " << received.error().msg() << std::endl;
    }

    usleep(50000);
  }

  return 0;
}
