//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <boost/asio.hpp>
#include <chrono>  // NOLINT(build/c++11)
#include <iomanip>
#include <iostream>

using boost::asio::ip::udp;
using std::cout;
using std::endl;
using std::string;

#include "libbpl_protocol/async_client.h"
#include "libbpl_protocol/constants.h"
#include "libbpl_protocol/expected.hpp"
#include "libbpl_protocol/io_service_thread.h"
#include "libbpl_protocol/packet.h"

using libbpl_protocol::AsynchronousClient;
using libbpl_protocol::Mode;
using libbpl_protocol::Packet;
using libbpl_protocol::PacketTypes;

typedef tl::expected<libbpl_protocol::Packet, std::string> PacketOrError;

int main(int argc, char **argv) {
  const string ipAddress(
      libbpl_protocol::DefaultBravoComputeIP);  // Default IP address for Bravo
  const unsigned int port = 6789;               // Default port for Bravo

  libbpl_protocol::IoServiceThread io_thread;
  AsynchronousClient bpl(io_thread.context(), ipAddress, port);
  std::chrono::time_point<std::chrono::steady_clock> time_last_send_;

  bpl.add_callback(libbpl_protocol::Packet::VerbosePacketRx);

  io_thread.start();

  const uint8_t deviceId = libbpl_protocol::BroadcastDeviceId;

  bpl.enable();

  //  } else if (true) {
  // Try the heartbeat functionality

  // for(int deviceId = 1; deviceId <= 7; deviceId++ ) {

  bpl.request(deviceId,
              {PacketTypes::SOFTWARE_VERSION, PacketTypes::MODEL_NUMBER,
               PacketTypes::SERIAL_NUMBER, PacketTypes::HEARTBEAT_SET,
               PacketTypes::HEARTBEAT_FREQUENCY});

  const uint8_t freq = 10;  // Hz
  bpl.setHeartbeatFrequency(deviceId, freq);

  bpl.setHeartbeatSet(deviceId, {PacketTypes::POSITION});

  usleep(1000);

  bpl.request(deviceId, {PacketTypes::POSITION, PacketTypes::VELOCITY,
                         PacketTypes::CURRENT, PacketTypes::HEARTBEAT_SET,
                         PacketTypes::HEARTBEAT_FREQUENCY});

  // Just wait
  sleep(5);

  bpl.setHeartbeatSet(deviceId, {});
  bpl.disable();

  io_thread.stop();
  io_thread.join();

  return 0;
}
