//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <iostream>

#include "libbpl_protocol/sync_client.h"

using libbpl_protocol::PacketTypes;

int main(int argc, char **argv) {
  const std::string ipAddress(
      libbpl_protocol::DefaultBravoComputeIP);  // Default IP address for Bravo
  const unsigned int port =
      libbpl_protocol::DefaultBravoPort;  // Default port for Bravo

  libbpl_protocol::SyncClient client(ipAddress, port);

  const uint8_t deviceId = 0x01;

  auto p = client.queryFloat(PacketTypes::SERIAL_NUMBER, deviceId);
  if (p) {
    std::cout << "Device: " << static_cast<int>(deviceId)
              << " :: Serial number = " << p.value() << std::endl;
  }

  p = client.position(deviceId);
  if (p) {
    std::cout << "Device: " << static_cast<int>(deviceId)
              << " :: Position =  " << p.value() << std::endl;
  }

  p = client.velocity(deviceId);
  if (p) {
    std::cout << "Device: " << static_cast<int>(deviceId)
              << " :: Velocity =  " << p.value() << std::endl;
  }

  p = client.current(deviceId);
  if (p) {
    std::cout << "Device: " << static_cast<int>(deviceId)
              << " ::  Current =  " << p.value() << std::endl;
  }
  return 0;
}
