//
// Copyright (c) 2022, University of Washington
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <boost/asio.hpp>
#include <iomanip>
#include <iostream>

using boost::asio::ip::udp;
using std::cout;
using std::endl;
using std::string;

#include "libbpl_protocol/async_client.h"
#include "libbpl_protocol/constants.h"
#include "libbpl_protocol/expected.hpp"
#include "libbpl_protocol/io_service_thread.h"
#include "libbpl_protocol/packet.h"

using libbpl_protocol::AsynchronousClient;
using libbpl_protocol::Mode;
using libbpl_protocol::Packet;
using libbpl_protocol::PacketTypes;

typedef tl::expected<libbpl_protocol::Packet, std::string> PacketOrError;

int main(int argc, char **argv) {
  const string ipAddress(
      libbpl_protocol::DefaultBravoComputeIP);  // Default IP address for Bravo
  const unsigned int port =
      libbpl_protocol::DefaultBravoPort;  // Default port for Bravo

  libbpl_protocol::IoServiceThread io_thread;
  AsynchronousClient bpl(io_thread.context(), ipAddress, port);
  std::chrono::time_point<std::chrono::steady_clock> time_last_send_;

  bpl.add_callback(libbpl_protocol::Packet::VerbosePacketRx);

  io_thread.start();

  // Turn off any existing heartbeat messages
  {
    Packet sent(PacketTypes::HEARTBEAT_FREQUENCY, 0xFF);
    sent.push_back<uint8_t>(0);
    bpl.send(sent);
    usleep(50000);
  }

  const uint8_t deviceId = 0xFF;

  time_last_send_ = std::chrono::steady_clock::now();

  // Set up heartbeat on all sensor -- use broadcast address
  {
    Packet sent(PacketTypes::HEARTBEAT_SET, deviceId);
    sent.push_back(PacketTypes::MODE);
    // sent.push_back<uint8_t>(PacketTypes::POSITION);
    // sent.push_back<uint8_t>(PacketTypes::VELOCITY);
    // sent.push_back<uint8_t>(PacketTypes::CURRENT);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    // sent.push_back<uint8_t>(0);
    bpl.send(sent);

    // sent.setDeviceId(0x06);
    // bpl.send(sent);
  }

  usleep(50000);

  bpl.enable();

  const uint8_t freq = 10;  // Hz
  bpl.setHeartbeatFrequency(deviceId, freq);

  {
    Packet sent(PacketTypes::REQUEST, deviceId);
    sent.push_back(PacketTypes::MODE);
    sent.push_back(PacketTypes::POSITION_LIMITS);
    sent.push_back(PacketTypes::CURRENT_LIMITS);
    sent.push_back(PacketTypes::HEARTBEAT_SET);
    sent.push_back(PacketTypes::HEARTBEAT_FREQUENCY);
    bpl.send(sent);
  }

  // Just wait
  const int cycles = 10;
  int count = 0;
  float sign = 1;
  bool is_done = false;
  while (!is_done) {
    // Sample for gripper (joint 1)
    {
      const uint8_t deviceId = 0x01;
      Packet sent(PacketTypes::POSITION, deviceId);
      const float pos = 10 + sign * 5;
      std::cout << "Sending position " << pos << std::endl;
      sent.push_back<float>(pos);
      bpl.send(sent);
    }

    // {  const uint8_t deviceId = 0x01;
    //   Packet sent(PacketTypes::VELOCITY, deviceId);
    //   const float vel = sign*10;
    //   std::cout << "Sending velocity " << vel << std::endl;
    //   sent.push_back<float>( vel );
    //   bpl.send(sent);
    // }

    // Sample for joint 2
    // const uint8_t deviceId = 0x02;
    // Packet sent(PacketTypes::POSITION, deviceId);
    //   sent.push_back<float>( 3.14 + sign*0.5 );
    // bpl.send(sent);

    sign *= -1;

    if (count++ > cycles) break;

    sleep(1);
  }

  io_thread.stop();
  io_thread.join();

  {
    Packet sent(PacketTypes::MODE, deviceId);
    sent.push_back(Mode::MODE_POSITION);
    bpl.send(sent);
  }

  // Turn off any existing heartbeat messages
  {
    Packet sent(PacketTypes::HEARTBEAT_SET, libbpl_protocol::BroadcastDeviceId);
    bpl.send(sent);
  }

  bpl.disable();

  return 0;
}
